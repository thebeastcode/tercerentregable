﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clinica.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Correo requerido")]
        [Display(Name = "Correo")]
        public string correo { get; set; }

        [Required(ErrorMessage = "Contraseña requerida")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "La {0} debe contener al menos {2} caracteres de longitud.", MinimumLength = 4)]
        [Display(Name = "Contraseña")]
        public string password { get; set; }
    }
}