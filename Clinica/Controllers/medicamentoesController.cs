﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;
using System.IO;

namespace Clinica.Controllers
{
    public class medicamentoesController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: medicamentoes
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View(db.medicamento.ToList());
        }

        // GET: medicamentoes/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamento medicamento = db.medicamento.Find(id);
            if (medicamento == null)
            {
                return HttpNotFound();
            }
            return View(medicamento);
        }

        // GET: medicamentoes/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: medicamentoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_medicamento,nombre,precio,imagen")] medicamento medicamento, HttpPostedFileBase ImageFile)
        {
            string filename = "";
            if (ImageFile != null && ImageFile.ContentLength > 0)
            {
                // extract only the fielname
                filename = Path.GetFileName(ImageFile.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads"), filename);
                ImageFile.SaveAs(path);
            }
            else
            {
                filename = "default-product-image.jpg";
            }
            medicamento.imagen = filename;
            if (ModelState.IsValid)
            {
                db.medicamento.Add(medicamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medicamento);
        }

        // GET: medicamentoes/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamento medicamento = db.medicamento.Find(id);
            if (medicamento == null)
            {
                return HttpNotFound();
            }
            return View(medicamento);
        }

        // POST: medicamentoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_medicamento,nombre,precio,imagen")] medicamento medicamento, HttpPostedFileBase ImageFile)
        {
            string filename = "";
            if (ImageFile != null && ImageFile.ContentLength > 0)
            {
                // extract only the fielname
                filename = Path.GetFileName(ImageFile.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads"), filename);
                ImageFile.SaveAs(path);
            }
            else
            {
                filename = "default-product-image.jpg";
            }
            medicamento.imagen = filename;
            if (ModelState.IsValid)
            {
                db.Entry(medicamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medicamento);
        }

        // GET: medicamentoes/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamento medicamento = db.medicamento.Find(id);
            if (medicamento == null)
            {
                return HttpNotFound();
            }
            return View(medicamento);
        }

        // POST: medicamentoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            medicamento medicamento = db.medicamento.Find(id);
            db.medicamento.Remove(medicamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
