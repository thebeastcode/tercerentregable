﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class usuariosController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();
        private ApplicationDbContext dc = new ApplicationDbContext();

        // GET: usuarios
        [Authorize(Roles = "Administrador")] 
        public ActionResult Index()
        {
            var usuario = db.usuario.Include(u => u.tipo_usuario);
            return View(usuario.ToList());
        }

        // GET: usuarios/Details/5
        [Authorize(Roles = "Administrador")] 
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: usuarios/Create
        [Authorize(Roles = "Administrador")] 
        public ActionResult Create()
        {
            ViewBag.id_tipo_usuario = new SelectList(db.tipo_usuario, "id_tipo_usuario", "tipo_usuario1");
            return View();
        }

        // POST: usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_usuario,nombre,apellido,correo,telefono,direccion,edad,password,id_tipo_usuario")] usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.usuario.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_tipo_usuario = new SelectList(db.tipo_usuario, "id_tipo_usuario", "tipo_usuario1", usuario.id_tipo_usuario);
            return View(usuario);
        }

        // GET: usuarios/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_tipo_usuario = new SelectList(db.tipo_usuario, "id_tipo_usuario", "tipo_usuario1", usuario.id_tipo_usuario);
            return View(usuario);
        }

        // POST: usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_usuario,nombre,apellido,correo,telefono,direccion,edad,password,id_tipo_usuario")] usuario usuario)
        {
            if (ModelState.IsValid)
            {       
                
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_tipo_usuario = new SelectList(db.tipo_usuario, "id_tipo_usuario", "tipo_usuario1", usuario.id_tipo_usuario);
            return View(usuario);
        }

        // GET: usuarios/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            usuario usuario = db.usuario.Find(id);
            db.usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [HttpPost]
        public ActionResult RedirectManage(string id)
        {
            return RedirectToAction("AdminRolesUsers", "Manage", id);
        }
    }
}
