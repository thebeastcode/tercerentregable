﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class citasController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: citas
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var cita = db.cita.Include(c => c.clinica).Include(c => c.estado_cita).Include(c => c.usuario).Include(c => c.usuario1);
            return View(cita.ToList());
        }

        // GET: citas/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = db.cita.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            return View(cita);
        }

        // GET: citas/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.id_clinica = new SelectList(db.clinica, "id_clinica", "nombre");
            ViewBag.id_estado = new SelectList(db.estado_cita, "id_estado_cita", "estado");
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre");
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre");
            return View();
        }

        // POST: citas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_cita,fecha,id_clinica,id_usuario_cliente,id_usuario_medico,id_estado")] cita cita)
        {
            if (ModelState.IsValid)
            {
                db.cita.Add(cita);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_clinica = new SelectList(db.clinica, "id_clinica", "nombre", cita.id_clinica);
            ViewBag.id_estado = new SelectList(db.estado_cita, "id_estado_cita", "estado", cita.id_estado);
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_medico);
            return View(cita);
        }

        // GET: citas/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = db.cita.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_clinica = new SelectList(db.clinica, "id_clinica", "nombre", cita.id_clinica);
            ViewBag.id_estado = new SelectList(db.estado_cita, "id_estado_cita", "estado", cita.id_estado);
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_medico);
            return View(cita);
        }

        // POST: citas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_cita,fecha,id_clinica,id_usuario_cliente,id_usuario_medico,id_estado")] cita cita)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cita).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_clinica = new SelectList(db.clinica, "id_clinica", "nombre", cita.id_clinica);
            ViewBag.id_estado = new SelectList(db.estado_cita, "id_estado_cita", "estado", cita.id_estado);
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", cita.id_usuario_medico);
            return View(cita);
        }

        // GET: citas/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = db.cita.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            return View(cita);
        }

        // POST: citas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cita cita = db.cita.Find(id);
            db.cita.Remove(cita);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
