﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class lista_medicamentoController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: lista_medicamento
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var lista_medicamento = db.lista_medicamento.Include(l => l.medicamento).Include(l => l.receta);
            return View(lista_medicamento.ToList());
        }

        // GET: lista_medicamento/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lista_medicamento lista_medicamento = db.lista_medicamento.Find(id);
            if (lista_medicamento == null)
            {
                return HttpNotFound();
            }
            return View(lista_medicamento);
        }

        // GET: lista_medicamento/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre");
            ViewBag.id_receta = new SelectList(db.receta, "id_receta", "id_receta");
            return View();
        }

        // POST: lista_medicamento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_receta,id_medicamento")] lista_medicamento lista_medicamento)
        {
            if (ModelState.IsValid)
            {
                db.lista_medicamento.Add(lista_medicamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", lista_medicamento.id_medicamento);
            ViewBag.id_receta = new SelectList(db.receta, "id_receta", "id_receta", lista_medicamento.id_receta);
            return View(lista_medicamento);
        }

        // GET: lista_medicamento/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lista_medicamento lista_medicamento = db.lista_medicamento.Find(id);
            if (lista_medicamento == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", lista_medicamento.id_medicamento);
            ViewBag.id_receta = new SelectList(db.receta, "id_receta", "id_receta", lista_medicamento.id_receta);
            return View(lista_medicamento);
        }

        // POST: lista_medicamento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_receta,id_medicamento")] lista_medicamento lista_medicamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lista_medicamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", lista_medicamento.id_medicamento);
            ViewBag.id_receta = new SelectList(db.receta, "id_receta", "id_receta", lista_medicamento.id_receta);
            return View(lista_medicamento);
        }

        // GET: lista_medicamento/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lista_medicamento lista_medicamento = db.lista_medicamento.Find(id);
            if (lista_medicamento == null)
            {
                return HttpNotFound();
            }
            return View(lista_medicamento);
        }

        // POST: lista_medicamento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            lista_medicamento lista_medicamento = db.lista_medicamento.Find(id);
            db.lista_medicamento.Remove(lista_medicamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
