﻿using Clinica.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clinica.Controllers;

namespace Clinicas.Controllers
{
    public class HomeController : Controller
    {
        
        private ClinicaDBEntities db = new ClinicaDBEntities();
        private ApplicationDbContext dc = new ApplicationDbContext();
        public ActionResult Index()
        {
            return RedirectToAction("Users", "Home");
        }

        public ActionResult Users()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login u)
        {
            var verifica = db.usuario.Where(a => a.nombre.Equals(u.correo) && a.password.Equals(u.password)).FirstOrDefault();
            if (verifica != null)
            {
                var roles = db.tipo_usuario.ToList();
                int id_user = 0;
                foreach (var r in roles)
                {
                    if (r.id_tipo_usuario == verifica.id_tipo_usuario)
                    {
                        Session["rol"] = r.id_tipo_usuario;
                        Session["name_user"] = verifica.nombre;
                        Session["valido"] = true;
                        Session["id"] = verifica.id_usuario;

                    }
                    id_user = verifica.id_usuario;
                }
                return RedirectToAction("ViewUser", "Home", new { id = id_user });
            }
            return View(u);
        }

        public ActionResult ConfirmUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmUser(usuario usr)
        {
            try
            {
                usuario usrs = new usuario();
                usrs.id_usuaro_asp = (string)(Session["id"]);
                usrs.nombre = usr.nombre;   
                usrs.apellido = usr.apellido;
                usrs.correo = (string) (Session["email"]);
                usrs.telefono = usr.telefono;
                usrs.edad = usr.edad;
                usrs.password = (string)(Session["pass"]);
                usrs.id_tipo_usuario = 1;
                usrs.direccion = usr.direccion;
                usrs.id_rol_asp = "1";
                db.usuario.Add(usrs);
                db.SaveChanges();
                
                string sesName =  (string)Session["email"];
                string id_usr = (string)  Session["id"];

                
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dc));
                var user = UserManager.FindByName(sesName);
                UserManager.AddToRole(user.Id, "Administrador");
                dc.SaveChanges();
                return RedirectToAction("Users", "Home");              
            }
            catch 
            {
                ViewBag.Message = "El registro no pudo ser guardado.";
            }
            
            return View();

        }

    }
}