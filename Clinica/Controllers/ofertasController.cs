﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class ofertasController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: ofertas
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {

            var ofertas = db.ofertas.Include(o => o.medicamento);
            return View(ofertas.ToList());
        }

        // GET: ofertas/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ofertas ofertas = db.ofertas.Find(id);
            if (ofertas == null)
            {
                return HttpNotFound();
            }
            return View(ofertas);
        }

        // GET: ofertas/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.id_medicamentos = new SelectList(db.medicamento, "id_medicamento", "nombre");
            return View();
        }

        // POST: ofertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_ofertas,id_medicamentos,descuento,fecha_inicio,fecha_fin")] ofertas ofertas)
        {
            if (ModelState.IsValid)
            {
                db.ofertas.Add(ofertas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_medicamentos = new SelectList(db.medicamento, "id_medicamento", "nombre", ofertas.id_medicamentos);
            return View(ofertas);
        }

        // GET: ofertas/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ofertas ofertas = db.ofertas.Find(id);
            if (ofertas == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_medicamentos = new SelectList(db.medicamento, "id_medicamento", "nombre", ofertas.id_medicamentos);
            return View(ofertas);
        }

        // POST: ofertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_ofertas,id_medicamentos,descuento,fecha_inicio,fecha_fin")] ofertas ofertas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ofertas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_medicamentos = new SelectList(db.medicamento, "id_medicamento", "nombre", ofertas.id_medicamentos);
            return View(ofertas);
        }

        // GET: ofertas/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ofertas ofertas = db.ofertas.Find(id);
            if (ofertas == null)
            {
                return HttpNotFound();
            }
            return View(ofertas);
        }

        // POST: ofertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ofertas ofertas = db.ofertas.Find(id);
            db.ofertas.Remove(ofertas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult OfertasCliente() 
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
