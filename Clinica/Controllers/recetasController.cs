﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class recetasController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: recetas
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var receta = db.receta.Include(r => r.usuario).Include(r => r.usuario1).Include(s => s.medicamento);
            return View(receta.ToList());
        }

        // GET: recetas/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receta receta = db.receta.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            return View(receta);
        }

        // GET: recetas/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre");
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre");
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre");
            return View();
        }

        // POST: recetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_receta,fecha_emitida,id_usuario_cliente,id_usuario_medico, id_medicamento, descripcion_item")] receta receta)
        {
            if (ModelState.IsValid)
            {
                db.receta.Add(receta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_medico);
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // GET: recetas/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receta receta = db.receta.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_medico);
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // POST: recetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_receta,fecha_emitida,id_usuario_cliente,id_usuario_medico, id_medicamento, descripcion_item")] receta receta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_usuario_cliente = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_cliente);
            ViewBag.id_usuario_medico = new SelectList(db.usuario, "id_usuario", "nombre", receta.id_usuario_medico);
            ViewBag.id_medicamento = new SelectList(db.medicamento, "id_medicamento", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // GET: recetas/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receta receta = db.receta.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            return View(receta);
        }

        // POST: recetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            receta receta = db.receta.Find(id);
            db.receta.Remove(receta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
