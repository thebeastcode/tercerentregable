﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class estado_citaController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: estado_cita
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View(db.estado_cita.ToList());
        }

        // GET: estado_cita/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estado_cita estado_cita = db.estado_cita.Find(id);
            if (estado_cita == null)
            {
                return HttpNotFound();
            }
            return View(estado_cita);
        }

        // GET: estado_cita/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: estado_cita/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_estado_cita,estado")] estado_cita estado_cita)
        {
            if (ModelState.IsValid)
            {
                db.estado_cita.Add(estado_cita);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_cita);
        }

        // GET: estado_cita/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estado_cita estado_cita = db.estado_cita.Find(id);
            if (estado_cita == null)
            {
                return HttpNotFound();
            }
            return View(estado_cita);
        }

        // POST: estado_cita/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_estado_cita,estado")] estado_cita estado_cita)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_cita).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_cita);
        }

        // GET: estado_cita/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estado_cita estado_cita = db.estado_cita.Find(id);
            if (estado_cita == null)
            {
                return HttpNotFound();
            }
            return View(estado_cita);
        }

        // POST: estado_cita/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_cita estado_cita = db.estado_cita.Find(id);
            db.estado_cita.Remove(estado_cita);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
