﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clinica.Models;

namespace Clinica.Controllers
{
    public class cotizacionsController : Controller
    {
        private ClinicaDBEntities db = new ClinicaDBEntities();

        // GET: cotizacions
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var cotizacion = db.cotizacion.Include(c => c.medicamento).Include(c => c.tipo_cristal).Include(c => c.tipo_lente).Include(c => c.tratamiento_lentes).Include(c => c.usuario);
            return View(cotizacion.ToList());
        }

        // GET: cotizacions/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cotizacion cotizacion = db.cotizacion.Find(id);
            if (cotizacion == null)
            {
                return HttpNotFound();
            }
            return View(cotizacion);
        }

        // GET: cotizacions/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.id_marca = new SelectList(db.medicamento, "id_medicamento", "nombre");
            ViewBag.id_tipo_cristal = new SelectList(db.tipo_cristal, "id_tipo_cristal", "nombre_tipo_cristal");
            ViewBag.id_tipo_lente = new SelectList(db.tipo_lente, "id_tipo_lente", "nombre_tipo_lente");
            ViewBag.id_tratamiento = new SelectList(db.tratamiento_lentes, "id_tratamiento", "nombre_tratamiento");
            ViewBag.id_usuario = new SelectList(db.usuario, "id_usuario", "nombre");
            return View();
        }

        // POST: cotizacions/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_cotizacion,id_usuario,od_esf,od_cil,od_a,oi_esf,oi_cil,oi_a,id_tipo_lente,id_tipo_cristal,id_tratamiento,id_marca,modelo_armazon")] cotizacion cotizacion)
        {
            if (ModelState.IsValid)
            {
                db.cotizacion.Add(cotizacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_marca = new SelectList(db.medicamento, "id_medicamento", "nombre", cotizacion.id_marca);
            ViewBag.id_tipo_cristal = new SelectList(db.tipo_cristal, "id_tipo_cristal", "nombre_tipo_cristal", cotizacion.id_tipo_cristal);
            ViewBag.id_tipo_lente = new SelectList(db.tipo_lente, "id_tipo_lente", "nombre_tipo_lente", cotizacion.id_tipo_lente);
            ViewBag.id_tratamiento = new SelectList(db.tratamiento_lentes, "id_tratamiento", "nombre_tratamiento", cotizacion.id_tratamiento);
            ViewBag.id_usuario = new SelectList(db.usuario, "id_usuario", "nombre", cotizacion.id_usuario);
            return View(cotizacion);
        }

        // GET: cotizacions/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cotizacion cotizacion = db.cotizacion.Find(id);
            if (cotizacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_marca = new SelectList(db.medicamento, "id_medicamento", "nombre", cotizacion.id_marca);
            ViewBag.id_tipo_cristal = new SelectList(db.tipo_cristal, "id_tipo_cristal", "nombre_tipo_cristal", cotizacion.id_tipo_cristal);
            ViewBag.id_tipo_lente = new SelectList(db.tipo_lente, "id_tipo_lente", "nombre_tipo_lente", cotizacion.id_tipo_lente);
            ViewBag.id_tratamiento = new SelectList(db.tratamiento_lentes, "id_tratamiento", "nombre_tratamiento", cotizacion.id_tratamiento);
            ViewBag.id_usuario = new SelectList(db.usuario, "id_usuario", "nombre", cotizacion.id_usuario);
            return View(cotizacion);
        }

        // POST: cotizacions/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_cotizacion,id_usuario,od_esf,od_cil,od_a,oi_esf,oi_cil,oi_a,id_tipo_lente,id_tipo_cristal,id_tratamiento,id_marca,modelo_armazon")] cotizacion cotizacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cotizacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_marca = new SelectList(db.medicamento, "id_medicamento", "nombre", cotizacion.id_marca);
            ViewBag.id_tipo_cristal = new SelectList(db.tipo_cristal, "id_tipo_cristal", "nombre_tipo_cristal", cotizacion.id_tipo_cristal);
            ViewBag.id_tipo_lente = new SelectList(db.tipo_lente, "id_tipo_lente", "nombre_tipo_lente", cotizacion.id_tipo_lente);
            ViewBag.id_tratamiento = new SelectList(db.tratamiento_lentes, "id_tratamiento", "nombre_tratamiento", cotizacion.id_tratamiento);
            ViewBag.id_usuario = new SelectList(db.usuario, "id_usuario", "nombre", cotizacion.id_usuario);
            return View(cotizacion);
        }

        // GET: cotizacions/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cotizacion cotizacion = db.cotizacion.Find(id);
            if (cotizacion == null)
            {
                return HttpNotFound();
            }
            return View(cotizacion);
        }

        // POST: cotizacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cotizacion cotizacion = db.cotizacion.Find(id);
            db.cotizacion.Remove(cotizacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
