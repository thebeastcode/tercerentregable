﻿using System.Web;
using System.Web.Optimization;

namespace Clinica
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/adminLTE/css").Include(
                "~/css/bootstrap.min.css",
                "~/css/AdminLTE.min.css",
                "~/css/skins/_all-skins.min.css",
                "~/css/jquery.ui.min.css",
                "~/css/font-awesome.min.css",
                "~/css/foundation-icons.min.css",
                "~/css/ionicons.min.css"));
            bundles.Add(new ScriptBundle("~/adminLTE/js").Include(
                "~/js/jquery.min.js",
                "~/js/bootstrap.min.js",
                "~/js/jqueryui.min.js",
                "~/js/app.min.js"));
            bundles.Add(new ScriptBundle("~/adminLTE/basicjs").Include(
                "~/js/jquery.min.js",
                "~/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/adminLTE/tablecss").Include(
                "~/css/bootstrap.min.css",
                "~/css/AdminLTE.min.css",
                "~/css/skins/_all-skins.min.css",
                "~/css/jquery.ui.min.css",
                "~/css/font-awesome.min.css",
                "~/css/foundation-icons.min.css",
                "~/css/ionicons.min.css",
                //"~/plugins/datatables/jquery.dataTables.min.css",
                "~/plugins/datatables/dataTables.bootstrap.css"));
            bundles.Add(new ScriptBundle("~/adminLTE/tablejs").Include(
                "~/js/jquery.min.js",
                "~/js/bootstrap.min.js",
                "~/js/jqueryui.min.js",
                "~/js/app.min.js",
                "~/plugins/datatables/jquery.dataTables.min.js",
                "~/plugins/datatables/dataTables.bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/user/indexusercss").Include(
                 "~/css/bootstrap.min.css",
                      "~/css/font-awesome.min.css",
                      "~/css/bootstrap-theme.css",
                      "~/css/da-slider.css",
                      "~/css/style.css"));
            bundles.Add(new ScriptBundle("~/user/indexuserjs").Include(
                     "~/js/modernizr-latest.js",
                      "~/js/jquery.min.js",
                      "~/js/bootstrap.min.js",
                      "~/js/jquery.cslider.js",
                      "~/js/headroom.min.js",
                      "~/js/jQuery.headroom.min.js",
                       "~/js/custom.js")); 
        }
    }
}
